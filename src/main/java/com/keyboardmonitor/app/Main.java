package com.keyboardmonitor.app;

public class Main {
    
    public static void main(String[] args) {
        KeyboardMonitor keyboardMonitor = new KeyboardMonitor();
        new FrameManager("KeyboardMonitor", keyboardMonitor.mainPanel);
        new GlobalKeyboardListener(keyboardMonitor.keysLog);
    }
}