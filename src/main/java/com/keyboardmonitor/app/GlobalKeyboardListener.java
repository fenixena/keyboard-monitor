package com.keyboardmonitor.app;

import java.util.Map.Entry;

import lc.kra.system.keyboard.GlobalKeyboardHook;
import lc.kra.system.keyboard.event.GlobalKeyAdapter;
import lc.kra.system.keyboard.event.GlobalKeyEvent;

import javax.swing.*;

class GlobalKeyboardListener {
    private static boolean run = true;

    GlobalKeyboardListener(JTextArea textArea) {
        KeysConstants keysConstants = new KeysConstants();
        GlobalKeyboardHook keyboardHook = new GlobalKeyboardHook(true); // Use false here to switch to hook instead of raw input

        System.out.println("Global keyboard hook successfully started, press [escape] key to shutdown. Connected keyboards:");

        for (Entry<Long, String> keyboard : GlobalKeyboardHook.listKeyboards().entrySet()) {
            System.out.format("%d: %s\n", keyboard.getKey(), keyboard.getValue());
        }

        keyboardHook.addKeyListener(new GlobalKeyAdapter() {

            @Override
            public void keyPressed(GlobalKeyEvent event) {
                String keyChar = keysConstants.getCharByCode(event.getVirtualKeyCode());
                String logEntry = "Key with code " + event.getVirtualKeyCode() + " ( " + keyChar + " ) has been pressed \n\r";
                textArea.append(logEntry);
                textArea.setCaretPosition(textArea.getText().length());

                if (event.getVirtualKeyCode() == GlobalKeyEvent.VK_ESCAPE) {
                    run = false;
                    textArea.append("Escape has been pressed, stop monitoring.");
                }
            }

            @Override
            public void keyReleased(GlobalKeyEvent event) {
                String keyChar = keysConstants.getCharByCode(event.getVirtualKeyCode());
                String logEntry = "Key with code " + event.getVirtualKeyCode() + " ( " + keyChar + " ) has been released \n\r";
                textArea.append(logEntry);
                textArea.setCaretPosition(textArea.getText().length());
            }
        });

        try {
            while(run) {
                Thread.sleep(128);
            }
        } catch(InterruptedException e) {
            //Do nothing
        } finally {
            keyboardHook.shutdownHook();
        }
    }

}


